const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const healthCheckRoute = require('./lib/src/health-check/routes');
const userRoute = require('./lib/src/user/routes');
const accountRoute = require('./lib/src/account/routes');
const marketRoute = require('./lib/src/market/routes');


const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

healthCheckRoute(app);
userRoute(app);
accountRoute(app);
marketRoute(app);

app.listen(config.get('app.port'),  () => {
  console.log(`Server listening on: ${config.get('app.port')}`);
})
