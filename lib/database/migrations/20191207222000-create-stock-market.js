module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('stockMarket', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      buyValue:{
        type: Sequelize.DOUBLE(3),
        allowNull: false
      },
      quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(250),
        allowNull: false
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user',
          key: 'id'
        }
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      queryInterface.addIndex(
        'stockMarket',
        { fields: ['id'], unique: true }
      );
    }),
  down: queryInterface => queryInterface.dropTable('stockMarket'),
};
