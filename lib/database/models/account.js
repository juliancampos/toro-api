module.exports = (sequelize, DataTypes) => {
  const account = sequelize.define('account', {
    value: {
      type: DataTypes.FLOAT
    },
    userId: {
      type: DataTypes.INTEGER
    },
    type: {
      type: DataTypes.STRING
    },
    balance: {
      type: DataTypes.FLOAT
    },
    description: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    },
    updatedAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    }
  }, {
    freezeTableName: true,
    tableName: 'account'
  });

  account.associate = (models) => {
    account.belongsTo(models.user, { foreignKey: 'userId' });
  };

  return account;
};
