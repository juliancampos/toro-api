module.exports = (sequelize, DataTypes) => {
  const stockMarket = sequelize.define('stockMarket', {
    name: {
      type: DataTypes.STRING
    },
    buyValue:{
      type: DataTypes.DOUBLE(3),
    },
    quantity: {
      type: DataTypes.INTEGER,
    },
    userId: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    },
    updatedAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    }
  }, {
    freezeTableName: true,
    tableName: 'stockMarket'
  });
  
  stockMarket.associate = (models) => {
    stockMarket.belongsTo(models.user, { foreignKey: 'userId' });
  };

  return stockMarket;
};
