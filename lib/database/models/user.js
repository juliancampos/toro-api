module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: {
      type: DataTypes.STRING
    },
    document: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    },
    updatedAt: {
      type: DataTypes.DATE(3),
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)')
    }
  }, {
    freezeTableName: true,
    tableName: 'user'
  });

  user.associate = (models) => {
    user.hasMany(models.account, { foreignKey: 'userId' });
  };

  return user;
};
