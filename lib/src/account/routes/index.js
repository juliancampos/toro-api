const { /* getAll, */ save } = require('../controllers');

module.exports = (app) => {
  // app.get('/account', getAll);
  app.post('/account', save);
}