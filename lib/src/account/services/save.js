const { account: model } = require('../../../database/models');
const repository = require('../../../database/repository');

const calcBalance = (data, lastMovimentation) => {
  if (!lastMovimentation) {
    return data.value;
  }

  if (data.type === 'add') {
    return lastMovimentation.balance + data.value;
  } else if (data.type === 'out') {
    if (data.value > lastMovimentation.balance) {
      throw { message: 'Valor de retirada excede saldo do usuário!' };
    }
    return lastMovimentation.balance - data.value;
  }

  throw { message: 'Tipo de movimentação inválido!' };
}

module.exports = async (data) => {
  data.balance = data.value;

  const lastMovimentation = await repository.findOne(model, {
    where: { userId: data.userId },
    order: [
      ['createdAt', 'DESC']
    ]
  });

  data.balance = calcBalance(data, lastMovimentation); 

  const result = await repository.create(model, data)
  return result;
}