const marketService = require('../services');

module.exports = async (req, res) => {
  try {
    const market = await marketService.get(req.params);
    return res.status(200).send(market);
  } catch (error) {
    return res.status(500).send(`Error: ${error.message}`);
  }
}