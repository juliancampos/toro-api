const save = require('./save');
const getAll = require('./get');
const sell = require('./sell');

module.exports = {
  save,
  getAll,
  sell
}