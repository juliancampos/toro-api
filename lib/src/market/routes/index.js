const { getAll, save, sell } = require('../controllers');

module.exports = (app) => {
  app.post('/market', save);
  app.get('/market/user/:id', getAll);
  app.post('/market/sell', sell);
}