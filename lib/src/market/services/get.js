const { stockMarket: model } = require('../../../database/models');
const repository = require('../../../database/repository');

module.exports = async (data) => {
  const listMarket = await repository.findAll(model, {
    where: { userId: data.id },
    order: [
      ['createdAt', 'DESC']
    ]
  });

  return listMarket;
}