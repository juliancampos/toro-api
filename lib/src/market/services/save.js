const { stockMarket: model } = require('../../../database/models');
const repository = require('../../../database/repository');
const accountService = require('../../account/services');

const registerOnAccount = async (data) => {
  await accountService.create({
    value: data.buyValue,
    type: "out",
    description: `Compra de ação: ${data.name}`,
    userId: data.userId
  });
}


module.exports = async (data) => {
  const dataFind = {
    userId: data.userId,
    name: data.name
  }
  const founded = await repository.findOne(model, { where: dataFind });

  if (!founded) {
    const result = await repository.create(model, data);
    await registerOnAccount(data);
    return result;
  }

  const result = await repository.update(model, { id: founded.id }, { quantity: founded.quantity + 1 });
  await registerOnAccount(data);

  return result;
}