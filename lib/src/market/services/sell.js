const { stockMarket: model } = require('../../../database/models');
const repository = require('../../../database/repository');
const accountService = require('../../account/services');

const registerOnAccount = async (data) => {
  await accountService.create({
    value: data.sellValue,
    type: "add",
    description: `Venda de ação: ${data.name}`,
    userId: data.userId
  });
}


module.exports = async (data) => {
  const dataFind = {
    id: data.id,
    name: data.name
  }
 
  const founded = await repository.findOne(model, { where: dataFind });

  if (!founded) {
    return 'Conta não encontrada!';
  }

  const result = await repository.update(model, { id: data.id }, { quantity: founded.quantity - 1 });
  await registerOnAccount(data);

  return result;
}