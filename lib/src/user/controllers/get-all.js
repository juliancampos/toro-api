const userService = require('../services');

module.exports = async (req, res) => {
  try {
    const result = await userService.getAll();
    return res.status(200).send(result);
  } catch (error) {
    return res.status(500).send(`Error: ${error.message}`);
  }
}