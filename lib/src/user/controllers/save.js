const userService = require('../services');

module.exports = async (req, res) => {
  try {
    const user = await userService.create(req.body);
    return res.status(200).send(user);
  } catch (error) {
    return res.status(500).send(`Error: ${error.message}`);
  }
}