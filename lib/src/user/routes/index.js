const { getAll, save } = require('../controllers');

module.exports = (app) => {
  app.get('/user', getAll);
  app.post('/user', save);
}