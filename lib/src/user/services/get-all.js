const {
  user: model,
  account
} = require('../../../database/models');
const repository = require('../../../database/repository');

const createQuery = (values) => (
  {
    include: [
      {
        model: account,
        required: false
      }
    ]
  });

module.exports = async (values = {}) => {
  const result = await repository.findAll(model, createQuery(values));
  return result;
}