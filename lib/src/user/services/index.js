const getAll = require('./get-all');
const create = require('./save');

module.exports = {
  getAll,
  create
}