const { user: model } = require('../../../database/models');
const repository = require('../../../database/repository');
const accountService = require('../../account/services');

module.exports = async (values) => {
  const user = await repository.create(model, values);
  accountService.create({
    value: 0,
    description: 'Inclusão de novo usuário',
    type: 'add',
    userId: user.id
  });
  return user;
}