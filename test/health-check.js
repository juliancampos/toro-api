const request = require('request');
const expect = require('chai').expect;

describe('Testar api ativa', function() {
  it('Deve retornar resposta ok da api', function(done) {

    const url = 'http://localhost:3000';

    request(url, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
});